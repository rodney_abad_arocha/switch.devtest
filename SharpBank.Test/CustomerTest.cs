﻿﻿using NUnit.Framework;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.00);
            savingsAccount.Deposit(4000.00);
            savingsAccount.Withdraw(200.00);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100,00\n" +
                    "Total $100,00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4.000,00\n" +
                    "  withdrawal $200,00\n" +
                    "Total $3.800,00\n" +
                    "\n" +
                    "Total In All Accounts $3.900,00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            oscar.OpenAccount(new Account(Account.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }
        [Test]
        public void TestExtractMoreMoneyThanExist() {            
            Account accountToExtract = new Account(Account.MAXI_SAVINGS);
            accountToExtract.Deposit(600.0);
            accountToExtract.Withdraw(500.0);
            Assert.AreEqual(100, accountToExtract.SumTransactions());

            ////I can't use TestMethod to create exceptions test

            ////This test show that we can't extract more money that exist in account...
            ///
            //accountToExtract.Deposit(600.0);
            //accountToExtract.Withdraw(700.0);                              --->> Here the exception break the test
            //Assert.AreEqual(-100, accountToExtract.SumTransactions());
        }

        [Test]
        public void TestTransactionBetwenAccount() {
            Customer Rodney = new Customer("Rodney");
            Account accountToExtract = new Account(Account.MAXI_SAVINGS);
            Account accountToDeposit = new Account(Account.CHECKING);
            accountToExtract.Deposit(500.0);
            accountToDeposit.Deposit(20.0);
            Rodney.OpenAccount(accountToExtract);
            Rodney.OpenAccount(accountToDeposit);
            Rodney.TransferBtwenAccounts(accountToExtract,accountToDeposit,400.0);
            Assert.AreEqual(100.0, accountToExtract.SumTransactions());
            Assert.AreEqual(420.0, accountToDeposit.SumTransactions());


            ////I can't use TestMethod to create exceptions test

            ////This test show that we can't transfer more money that exist in account...
            
            //Rodney.TransferBtwenAccounts(accountToExtract, accountToDeposit, 200.0);--->> Here the exception break the test
            //Assert.AreEqual(-100.0, accountToExtract.SumTransactions());
            //Assert.AreEqual(620.0, accountToDeposit.SumTransactions());
        }
    }
}
