﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private int accountType;
        private DateTime accountCreationDate;
        private List<Transaction> transactions;

        public Account(int accountType)
        {
            this.AccountType = accountType;
            accountCreationDate = DateProvider.GetInstance().Now();
            Transactions = new List<Transaction>();
        }

        public List<Transaction> Transactions { get => transactions; set => transactions = value; }
        public int AccountType { get => accountType; set => accountType = value; }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                Transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if (amount > SumTransactions())
                {
                    throw new ArgumentException("amount is higher than available");
                }
                else
                {
                    Transactions.Add(new Transaction(-amount));
                }
            }
        }
        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (AccountType)
            {                
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                // case SUPER_SAVINGS:
                //     if (amount <= 4000)
                //         return 20;
                case MAXI_SAVINGS:
                    bool flag = false;
                    DateTime today = DateTime.Now;
                    foreach (Transaction item in Transactions)
                    {
                        if (item.Amount<0)
                        {
                            if (item.TransactionDate.AddDays(10) > today)
                            {
                                flag = true;
                            }
                        }
                    }
                    if (flag==false)
                    {
                        return amount * 0.05;                        
                    }
                    return amount * 0.001;
                //if (amount <= 1000)
                //    return amount * 0.02;
                //if (amount <= 2000)
                //    return 20 + (amount - 1000) * 0.05;
                //return 70 + (amount - 2000) * 0.1;
                default:
                    return amount * 0.001;
            }
        }
        public double DailyInterestEarned() {
            int days = (DateTime.Now - accountCreationDate).Days + 1;
            double accumulated = 0;
            double dailyBase = 0;
            int i = 1;
            while (i <= days)
            {
                dailyBase = InterestEarned() / 365;
                accumulated += dailyBase;
                Deposit(dailyBase);
                i ++;
            }
            return accumulated;
        }

        public double SumTransactions()
        {            
            double amount = 0.0;
            if (Transactions.Count>0)
            {
                foreach (Transaction t in Transactions)
                    amount += t.Amount;
            }            
            return amount;
        }
        public int GetAccountType()
        {
            return AccountType;
        }

    }
}
