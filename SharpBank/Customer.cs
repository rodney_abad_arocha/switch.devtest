﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private string name;
        private DateTime indcriptionDate;
        private List<Account> accounts;

        public Customer(string name)
        {
            this.name = name;
            indcriptionDate= DateProvider.GetInstance().Now();
            accounts = new List<Account>();
        }

        public string GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }
        public DateTime GetInscriptionDate()
        {
            return this.indcriptionDate;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            if (accounts.Count>0)
            {
                foreach (Account a in accounts)
                    total += a.DailyInterestEarned();
            }
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            string statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private string StatementForAccount(Account a)
        {
            string s = "";
            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.Transactions)
            {
                s += "  " + (t.Amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.Amount) + "\n";
                total += t.Amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }
        private string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        public void TransferBtwenAccounts(Account accountToExtract, Account accountToDeposit, double ammount) {
            double availableAmmount = 0;
            foreach (Transaction item in accountToExtract.Transactions)
            {
                availableAmmount += item.Amount;
            }
            if ((availableAmmount>0)&&availableAmmount>ammount)
            {
                accountToExtract.Withdraw(ammount);
                accountToDeposit.Deposit(ammount);
            }
            else
            {
                throw new Exception("the extraction account doesn't have enough money");
            }
        }
    }
}
