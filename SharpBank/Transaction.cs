﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        private double amount;

        private DateTime transactionDate;

        public Transaction(double amount)
        {
            this.Amount = amount;
            TransactionDate = DateProvider.GetInstance().Now();
        }
        public double Amount { get => amount; set => amount = value; }
        public DateTime TransactionDate { get => transactionDate; set => transactionDate = value; }
    }
}
